import {default as xmlrpc} from 'xmlrpc';

export class Siptrack {

    constructor(default_options) {
        const options = Object.assign(
            {},
            {
                enable_tls: false,
                port: 9242,
                hostname: 'localhost',
                session_id: null,
                path: '/',
            },
            default_options
        );

        // Expose the options in a class property
        this.options = options;

        // Create xmlrpc.Client instance
        this.transport = xmlrpc.createClient({
            host: options.hostname,
            port: options.port,
            path: options.path
        });
    }


    call(command, data=[]) {
        return new Promise((resolve, reject) => {
            this.transport.methodCall(command, data, (error, value) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(value);
                }
            });
        });
    }

    
    login(username, password) {
        return new Promise((resolve, reject) => {
            this.call('login', [username, password]).then(value => {
                this.session_id = value;
                resolve(value);
            }).catch(error => {
                reject(error);
            });
        });
    }


    logout() {
        return new Promise((resolve, reject) => {
            this.call('logout', [this.session_id]).then(value => {
                this.session_id = null;
                resolve(value);
            }).catch(error => {
                reject(error);
            });
        })
    }


    list_sessions() {
        return this.call('list_sessions', [this.session_id]);
    }


    kill_session(session_id) {
        return this.call('kill_session', [this.session_id, session_id]);
    }

}