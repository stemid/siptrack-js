class ExtendableError extends Error {
    constructor(message) {
      super(message);
      this.name = this.constructor.name;
      if (typeof Error.captureStackTrace === 'function') {
        Error.captureStackTrace(this, this.constructor);
      } else { 
        this.stack = (new Error(message)).stack; 
      }
    }
  }  

export default class SiptrackError extends ExtendableError {
    constructor(message) {
        super(message);
    }
}

module.exports = {
    SiptrackError: SiptrackError
}