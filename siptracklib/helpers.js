export function set_property(obj, prop_name, prop_value) {
    if (!obj.hasOwnProperty(prop_name)) {
        Object.defineProperty(obj, prop_name, {
            enumerable: false,
            configurable: false,
            writable: false,
            value: prop_value
        });
    }
}